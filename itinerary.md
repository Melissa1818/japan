Booking.com: use kortingslink:
------
    - https://www.booking.com/s/18_6/bcfaf0af - van samuel voor melissa
    - https://www.booking.com/s/18_6/d2bb81c4 - melissa voor sam

Days:
-------    
    - Kumano kodo: http://www.tb-kumano.jp/en/kumano-kodo/suggested-walks/#takijiri-hongu

Nights:
--------
    - Tokyo: 2
    
    - Kyoto: 3
    
    - *Tanabe 1 BOOKED*
        - https://www.booking.com/hotel/jp/the-cue.en-gb.html - nicer pictures, possibly no shared bathroom
        - Samuel's Booking Account
        - cancellable untill 12/10
        - booked 20/10-21/10, 10500 yen = 82 eur + 15euro cashback
    
    - Nahahecki area: 1
        - http://www.kumano-travel.com/index/en/action_ContentsList_SearchExec/init1/s_classification1/s_area_id9
        -
    - Hongu area: 1
        - https://www.booking.com/hotel/jp/omuraya-no3.en-gb.html?label=gen173nr-1FCAEoggJCAlhYSDNYBGipAYgBAZgBLsIBA3gxMcgBD9gBAegBAfgBC5ICAXmoAgM;sid=b540603b33992b485acc58e4f9c511c6;all_sr_blocks=241392103_102328628_3_2_0;checkin=2018-10-22;checkout=2018-10-23;dest_id=-229710;dest_type=city;dist=0;group_adults=3;group_children=0;hapos=3;highlighted_blocks=241392103_102328628_3_2_0;nflt=ht_id%3D209%3B;no_rooms=1;req_adults=3;req_children=0;room1=A%2CA%2CA;sb_price_type=total;srepoch=1528547028;srfid=f60e67a7644b62432922b382c77f191a2dd1238fX3;srpvid=436e57299cf10161;type=total;ucfs=1&#hotelTmpl
        - https://www.booking.com/hotel/jp/sansuikan-kawayu-matsuya.en-gb.html?label=gen173nr-1FCAEoggJCAlhYSDNYBGipAYgBAZgBLsIBA3gxMcgBD9gBAegBAfgBC5ICAXmoAgM;sid=b540603b33992b485acc58e4f9c511c6;all_sr_blocks=224068701_98736297_0_0_0%2C224068702_98736297_0_0_0;checkin=2018-10-22;checkout=2018-10-23;dest_id=-229710;dest_type=city;dist=0;group_adults=3;group_children=0;hapos=6;highlighted_blocks=224068702_98736297_0_0_0%2C224068701_98736297_0_0_0;no_rooms=2;req_adults=3;req_children=0;room1=A;room2=A%2CA;sb_price_type=total;srepoch=1528547143;srfid=194651872c4da2b3f89e623a083cac75aa09004fX6;srpvid=18385763b96b0469;type=total;ucfs=1&#hotelTmpl

    - Koyasan: 1
        - https://www.japaneseguesthouses.com/ryokan-search-results/?area=Mt%20Koya
        - https://www.japaneseguesthouses.com/ryokan-single/?ryokan=Shojoshin-in
        - https://www.japaneseguesthouses.com/ryokan-single/?ryokan=Hoon-in
        - https://www.japaneseguesthouses.com/ryokan-single/?ryokan=Muryoko-in
    - *Nara: 1 BOOKED*
        - https://www.booking.com/hotel/jp/family-inn-nara.nl.html
        - Melissa's Booking Account
        - cancellable untill 16/10
        - booked 24/10-25/10, 10800 yen = 85 eur + 15euro cashback
    - Hiroshima: 2 BOOKED
        - https://www.booking.com/hotel/jp/hondori-inn.en-gb.html 
        - melissa booking
        - 50% refundable + 50% paid by samuel + 15eu cashback
    
    - Hakone/tokyo: 1
    
    - Tokyo: 2

Itinerary including Tokyo, Kyoto, Tanabe, Hiroshima, Nara, Hongu (area), Kumona Kodo Hike, Hakone, Koya-san, Nakahechi (area).

Arrival
-------
15/10 07:50 Tokyo.

15/10
-----

day: full day tokyo,

night: dinner + sleep in tokyo

16/10
------

day: full day tokyo

night: dinner + sleep in tokyo

17/10
------

day: part day tokyo, travel to kyoto 3,5 hrs

night: dinner + sleep in kyoto

18/10
-------

day: full day kyoto

night: sleep in kyoto

19/10
-------

day: full day kyoto

night: sleep in kyoto

20/10
------

day: part day kyoto + travel to tanabe 3,5hrs

night: dinner + sleep in tanabe https://www.booking.com/hotel/jp/the-cue.en-gb.html (Samuel's Booking Account)

21/10
------

day: bus to takijiri 30mins + walk kumano kodo to nakahechi area 6hrs (food)

night: dinner + sleep nahahecki area

22/10
------

day: walk kumano kodo to hongu area 8hrs (food!)

night: dinner + sleep hongu area

23/10
-----

day: travel to gojo 4hrs + travel to koyasan 1,5hrs + koyasan part day

night: dinner + sleep koyasan (temple hotel?)

24/10
-----

day: travel to nara 3,5hrs + nara part day

night: dinner + sleep nara https://www.booking.com/hotel/jp/family-inn-nara.nl.html (melissa's booking account)

25/10
-------

day: travel to hiroshima 3hrs + part day hiroshima

night: dinner + sleep hiroshima https://www.booking.com/hotel/jp/hondori-inn.en-gb.html (melissa booking)

26/10
--------

day: full day hiroshima (miyajima island?)

night: dinner + sleep hiroshima https://www.booking.com/hotel/jp/hondori-inn.en-gb.html (melissa booking)

27/10
------

day: travel to hakone 5hrs + part day hakone

night:
    a) dinner + sleep hakone
    b) dinner hakone + travel tokyo 15mins + sleep tokio
    c) travel tokyo 15mins + dinner + sleep tokyo

28/10
------

day:
    a) part day hakone + travel to tokio 15mins
    b/c) full day tokyo

night: dinner + sleep tokyo

29/10
------

day: full day tokyo

night: dinner + sleep tokyo

30/10
------

day: go to airport at 09:00

night: die in nederland
